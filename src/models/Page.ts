import { DbEntity } from "./DbEntity";

export type PageId = Page["_id"];

export interface Page extends DbEntity {
    title: string;
    visible?: boolean;
    content: string;
}

