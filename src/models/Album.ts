import { DbEntity } from "./DbEntity";
import { FileId } from "./File";

export type AlbumId = Album["_id"];

export interface Album extends DbEntity {
    parent?: AlbumId;
    title: string;
    visible?: boolean;
    ordinal: number;
    files: AlbumFile[];
}

export interface AlbumFile {
    visible?: boolean;
    file: FileId;
    ordinal: number;
}

