import { AlbumId } from "./Album";
import { DbEntity } from "./DbEntity";
import { PageId } from "./Page";

export type MenuNodeId = MenuNode["_id"];

export enum MenuNodeType {
    GROUP = "GROUP",
    ITEM = "ITEM"
}

export interface MenuNode extends DbEntity {
    parent?: MenuNodeId;
    nodeType: MenuNodeType;
    title: string;
    visible: boolean;
    ordinal: number;
}

export interface MenuGroup extends MenuNode {
    nodeType: MenuNodeType.GROUP;
}

export enum MenuItemEntity {
    PAGE = "PAGE",
    ALBUM = "ALBUM"
}

export interface MenuItem<Entity extends MenuItemEntity> extends MenuNode {
    nodeType: MenuNodeType.ITEM;
    entityType: Entity;
    entityId: Entity extends MenuItemEntity.ALBUM ? AlbumId : PageId;
}

export interface PageMenuItem extends MenuItem<MenuItemEntity.PAGE> {}

export interface AlbumMenuItem extends MenuItem<MenuItemEntity.ALBUM> {}
