import { DbEntity, DbEntityId } from "./DbEntity";

export enum FileType {
    IMAGE = "IMAGE",
    VIDEO = "VIDEO",
    AUDIO = "AUDIO",
    HTML = "HTML",
}

export type FileId = DbEntityId;

export type FileMetadata<Type extends FileType> = Type extends FileType.IMAGE
    ? ImageMetadata
    : Type extends FileType.VIDEO
        ? VideoMetadata
        : Type extends FileType.AUDIO
            ? AudioMetadata
            : Type extends FileType.HTML ? HtmlMetadata : any;

export interface File<Type extends FileType = FileType> extends DbEntity {
    name: string; // friendly name
    path: string;
    metadata: FileMetadata<Type>;
    description?: string;
    type: Type;
}

export type ImageFile = File<FileType.IMAGE>;
export type HtmlFile = File<FileType.HTML>;
export type AudioFile = File<FileType.AUDIO>;
export type VideoFile = File<FileType.VIDEO>;

export interface ImageMetadata {
    width: number;
    height: number;
    type: string; // e.g.: "jpg"
}

export interface VideoMetadata {
    width: number;
    height: number;
    duration: number;
    type: string; // e.g.: "mp4"
}

export interface AudioMetadata {
    duration: number;
    type: string; // e.g.: "mp3"
}

export interface HtmlMetadata {}
