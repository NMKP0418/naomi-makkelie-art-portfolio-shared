export type DbEntityId = string;

export interface DbEntity {
    _id: DbEntityId;
}
